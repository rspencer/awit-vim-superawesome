filetype on                             " filetypes based on extension
syntax on                               " turn syntax highlighting on

set backspace=indent,eol,start          " allow backspacing over everything in insert mode
set cindent                             " C code indentation
set cinkeys=0{,0},0),:,!^F,o,O,e
set cinoptions=>s,e0,n0,f0,{0,}0,^0,L-1,:s,=s,ls,b0,gs,hs,N0,ps,t0,is,+2s,c1,C1,/0,(2s,us,Us,w0,W2s,k2s,m1,M-1,j0,J0,)20,*70,#1
set cursorline                          " highlight current line
set display=uhex                        " all unknown characters should be in hex
set fileformats=unix                    " only support Unix line endings
set hlsearch                            " highlight search results
set ignorecase                          " ignore search case
set laststatus=2                        " always show status line
set mouse=                              " disable mouse in vim, so we can actually copy paste between applications
set nobackup                            " don't use backup files
set noerrorbells                        " no annoying sound on errors
set nostartofline                       " try keep on same column when moving in a file
set novisualbell
set nowritebackup                       " don't use backup files
set ruler                               " always show the cursor position
set shiftwidth=4                        " when shifting, indent using four spaces
set showcmd                             " show incomplete commands
set showmatch                           " show matching brackets when text indicator is over them
set signcolumn=yes                      " always have the sign column
"set spell                              " enable spell checking
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
set t_Co=256                            " setup colors
set t_vb=                               " no annoying sound on errors
set tabstop=4                           " number of visual spaces per TAB
set title                               " set the window's title, reflecting the file currently being edited
set ttimeoutlen=500                     " wait up to 500ms for key sequences to complete

"
" Highlight colors
" https://vim.fandom.com/wiki/Xterm256_color_names_for_console_Vim
"

" Set autocomplete menu colors
" grey89 / grey7
highlight Pmenu ctermfg=254 ctermbg=233
" grey7 / grey89
highlight PmenuSel cterm=bold ctermfg=233 ctermbg=254

" Set search background
" lightgoldenrod2 / red3
highlight search cterm=bold ctermbg=222 ctermfg=160

" Set color of cursor column
" grey27
highlight CursorColumn ctermbg=238

" Set color of end of line for programming
" grey27
highlight ColorColumn ctermbg=238

" Highlight line we're on
" grey15
highlight CursorLine cterm=none ctermbg=235

" Highlight messed up tab/spaces
" orangered1
highlight ExtraWhitespaces ctermbg=202

" Switch whitespace handling based on file type {
function! WhitespaceHighlight()
  if &filetype == 'python'
    call matchadd('ExtraWhitespaces','^\t')
    set colorcolumn=132
  elseif &filetype == 'yaml'
    call matchadd('ExtraWhitespaces',' \t')
    call matchadd('ExtraWhitespaces','\t ')
    set colorcolumn=132
  elseif &filetype != ''
    call matchadd('ExtraWhitespaces',' \t')
    call matchadd('ExtraWhitespaces','\t ')
    call matchadd('ExtraWhitespaces','^  ')
    set colorcolumn=132
  endif
  call matchadd('ExtraWhitespaces','\s\+$')
endfunction
" }

" Set file type detection
autocmd BufNewFile,BufRead * call WhitespaceHighlight()
autocmd BufNewFile,BufRead *.html.ep set filetype=php | call WhitespaceHighlight()
autocmd BufNewFile,BufRead *.md set filetype=markdown | call WhitespaceHighlight()
" Convert tabs to spaces, a tab is converted to 4 spaces
autocmd BufNewFile,BufRead *.py set filetype=python | set expandtab softtabstop=4 | call WhitespaceHighlight()
autocmd BufNewFile,BufRead *.sls set filetype=python | set expandtab softtabstop=4 | call WhitespaceHighlight()

"
" Plugin configuration
"

" neocomplete {
let g:neocomplete#enable_at_startup = 1
" }

" syntastic {
let g:syntastic_python_checkers = ['python', 'pylama']
" }

" vim-airline {
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
" }

" vim: set filetype=vim expandtab shiftwidth=2 tabstop=2 :
